import Foundation

let filePath = [
    FileManager.default.currentDirectoryPath,
    "localizations.tsv"
].joined(separator:"/")

let fileURL = URL(fileURLWithPath: filePath)
let tsv = try! String(contentsOf: fileURL, encoding: .utf8)

let rows = tsv.components(separatedBy: .newlines)
let languages = rows.first!.split(separator: "\t").dropFirst()
let translations = rows.dropFirst()
    .filter{ !$0.isEmpty }
    .map{ 
        $0
        .split(separator: "\t")
        .map{ $0.trimmingCharacters(in: .whitespaces) } 
        .filter { !$0.isEmpty }
    }
    .filter { !$0.isEmpty }

languages
    .map{ String($0) }
    .enumerated().forEach{ index, lang in
        Format.allCases.forEach{ format in
            let keyValuePair = translations
                .sorted(by: { s1, s2 in String(s1[0]) < String(s2[0]) })
                .compactMap{ translation -> String? in
                    guard let key = translation.first else { return nil }
                    let defaultValue = translation.dropFirst().first ?? key
                    let value = String(translation.dropFirst(index + 1).first ?? defaultValue)
                    return format.row(key: String(key), value: value)
                }

            let export = format.filePath(lang:lang)
            do {
                try format
                    .fileContent(translations: keyValuePair)
                    .write(to: export, atomically: true, encoding: .utf8)
                print("Exported to \(export)")
            } catch {
                print("Error occurred in export to \(export)\n\(error)")
            }
        }
    }

enum Format: String, CaseIterable {
    case iOS
    case Android
    case Flutter
}

extension Format {

    func filePath(lang: String) -> URL {
        let fileName: String = {
            switch self {
            case .iOS:
                return "\(lang).strings"
            case .Android:
                return "\(lang).xml"
            case .Flutter:
                return "\(lang).arb"
            }
        }()
        let filePath = [
            FileManager.default.currentDirectoryPath,
            rawValue,
            fileName
        ].joined(separator: "/")
        return URL(fileURLWithPath: filePath)
    }

    func fileContent(translations: [String]) -> String {
        switch self {
        case .iOS:
            return translations.joined(separator: "\n")
        case .Android:
            return """
            <resources xmlns:tools="http://schemas.android.com/tools">
            \(translations.map{ "\t\($0)" }.joined(separator: "\n"))
            </resources>
            """
        case .Flutter:
            return """
            {
            \(translations.joined(separator: ",\n"))
            }
            """
        }
    }

    private func prefix(_ value: String) -> String {
        switch self {
        case .iOS, .Flutter:
            return ""
        case .Android:
            return value.contains("&") ? "<![CDATA[" : ""
        }
    }

    private func suffix(_ value: String) -> String {
        switch self {
        case .iOS, .Flutter:
            return ""
        case .Android:
            return value.contains("&") ? "]]>" : ""
        }
    }

    private func platformValue(_ value: String) -> String {
        return prefix(value) + value + suffix(value)
    }

    func row(key: String, value: String) -> String {
        switch self {
        case .Flutter:
            return "\t\"\(key)\": \"\(value)\""
        case .iOS:
            return [
                key,
                value
                    .replacingOccurrences(of: "\"", with: "\\\"")
                    .replacingOccurrences(of: "%s", with: "%@")
                    .replacingOccurrences(of: "$s", with: "$@")
            ]
            .map{ "\"\($0)\"" }
            .joined(separator: "=") + ";"
        case .Android:
            return """
            <string name="\(key)">\(platformValue(value.replacingOccurrences(of: "'", with: "\\'")))</string>
            """
        }
    }

}
